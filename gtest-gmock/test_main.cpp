#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "turtle.hpp"
#include "painter.hpp"

class MockTurtle : public Turtle {
public:
    MOCK_METHOD(void, PenUp, (), (override));
};

TEST(PainterTest, CanDrawSomething) {
    MockTurtle turtle;
    EXPECT_CALL(turtle, PenUp());
    Painter painter(turtle);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
