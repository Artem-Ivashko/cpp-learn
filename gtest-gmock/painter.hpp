#ifndef PAINTER_HPP
#define PAINTER_HPP

#include "turtle.hpp"

class Painter {
public:
    Painter(Turtle& turtle);
};

#endif
