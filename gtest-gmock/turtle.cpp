#include <iostream>

#include "turtle.hpp"

using namespace std;

Turtle::Turtle() {
    cout << "A new turtle is created!" << endl;
}

void Turtle::PenUp() {
    cout << "I am the real turtle!" << endl;
}
