#ifndef TURTLE_HPP
#define TURTLE_HPP

class Turtle {
public:
    virtual ~Turtle() {}
    Turtle();
    virtual void PenUp();
};

#endif
