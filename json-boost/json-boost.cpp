#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <map>

#include <boost/json.hpp>
#include <boost/json/value.hpp>

using namespace std;

boost::json::value parse_json_file(string file_path) {
    ifstream ifs {file_path};
    if (!ifs)
        cerr << "Could not open the JSON file " << file_path << endl;
    ostringstream json_contents;
    json_contents << ifs.rdbuf();
    cout << "The contents of the JSON file " << file_path << " is:"
         << endl << json_contents.str() << endl;
    return boost::json::parse(json_contents.str());
}


int main() {
    boost::json::value result = parse_json_file("input.json");
    // Check that the root is a JSON object, and not, say, a vector
    assert(result.is_object());
    const auto object_pointer = result.as_object();
    for (auto key_value_pair: object_pointer) {
        cout << "Key: " << key_value_pair.key() << endl
             << "Value: " << key_value_pair.value() << endl;
    }
    // Reading the whole (nested) structure at once into
    // the suitable custom type
    using my_nested_type = map<string, vector<map<string, string>>>;
    my_nested_type nest = boost::json::value_to<my_nested_type>(result);
    for(auto m1: nest) {
        cout << "Key at root: " << m1.first << endl;
        for(auto m2: m1.second)
            for(auto m3: m2)
                cout << "Low-level key: " << m3.first
                     << ", corresponding value: " << m3.second << endl;
    }
}
