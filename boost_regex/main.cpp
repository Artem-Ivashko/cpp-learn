#include <string>
#include <iostream>

#include <boost/regex.hpp>

using namespace std;

int main() {
  string regex_string = ".*next_server\\s=\\s(.*)\\s*";
  cout << "Our regular expression string: " << regex_string << endl;
  const boost::regex regular_expression(regex_string);
  boost::smatch match_details;
  string line = "DHCP4.OPTION[5]:      next_server = 192.26.36.54";
  // result tells if there was any match or not
  bool result = boost::regex_search(line, match_details, regular_expression);
  if (result) {
    // The IP address
    string match(match_details[1].first, match_details[1].second);
    cout << "Our match: " << match << endl;
  }
}
